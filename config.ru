require 'sprockets'
require_relative 'sequel_cuba'

map '/client-app' do
  env = Sprockets::Environment.new
  env.append_path 'js/'
  run env
end

map '/assets' do
  env = Sprockets::Environment.new
  env.append_path 'js/assets/'
  run env
end

run Cuba
