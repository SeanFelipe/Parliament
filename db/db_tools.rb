require 'pg'
require 'sequel'
require_relative 'config'


TEST_STUDENT_RECORDS = [
  ['Helen', 'Folasade', 14],
  ['Sally', 'McNally', 15],
  ['Bruce', 'Banner', 14],
  ['Malcolm', 'Petrovic', 13],
  ['Judy', 'Santos', 13],
  ['Marcel', 'Ross', 17],
  ['John', 'Wick', 16],
  ['Jeff', 'Williams', 18],
  ['Ruby', 'Matsumoto', 18],
  ['Tiffany', 'Zhu', 16],
  ['Ally', 'Wang', 15],
  ['Robert', 'Mack', 14],
  ['Alexandra', 'Ling', 14],
  ['Frank', 'Dux', 17],
  ['Kate', 'Ripley', 14],
]


def start_postgresql_server
  system('pg_ctl -D ./parliament.pg/ start')
end
alias :sp :start_postgresql_server

def init
  @db = Sequel.connect(DBURL)
end

def drop_students_table
  @db << 'DROP TABLE students; COMMIT'
end

def create_students_table
  @db.create_table :students do
    primary_key :id

    # student
    String :firstName
    String :lastName
    Integer :age
    Boolean :checkedIn
    String :designatedBus

    # other
    String :staffNotes

    # who were they dropped off by?
    String :droppedOffByName
    String :droppedOffByPhone
    String :droppedOffByEmail
  end
end

def insert_student(fn, ln, age)
  @students.insert(
    :firstName => fn,
    :lastName => ln,
    :age => age,
    :checkedIn => false,
    :droppedOffByName => 'Juana Smith',
    :droppedOffByPhone => '704-555-1212',
    :droppedOffByEmail => 'juana@example.com',
  )
end

def rebuild_student_records
  init

  drop_students_table
  create_students_table

  @students = @db[:students]
  TEST_STUDENT_RECORDS.each {|ss| insert_student(ss[0], ss[1], ss[2])}
end

def first
  @students.first
end
