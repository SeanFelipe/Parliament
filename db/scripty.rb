require 'httparty'
require 'byebug'; alias :breakpoint :byebug

CUB = 'http://localhost:9292'

body = {
  :firstName => 'Marjorie',
  :lastName  => 'Mathematica',
  :age => 21,
}

r = HTTParty.post("#{CUB}/addStudent", :body => body)

breakpoint
puts
