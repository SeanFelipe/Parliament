require 'cuba'
require 'json'
require 'sequel'
require 'ostruct'
require 'byebug'; alias :breakpoint :byebug # agb ignore
require_relative 'html/index'
require_relative 'db/db_setup'
require_relative 'db/config'


DB = Sequel.connect(DBURL)
students = DB[:students]


Cuba.use Rack::Static, :urls => ['/images']

Cuba.define do
  on get do
    res.headers.store('Access-Control-Allow-Origin', '*')

    on root do
      res.redirect 'demo'
    end

    on 'demo' do
      res.write INDEX_HTML
    end

    on 'studentList' do
      rr = students.all.to_json
      puts rr
      res.write rr
    end

    on 'checkIn/:id' do |sid|
      students.where(:id => sid.to_i).update(:checkedIn => true)
      res.write students.where(:id => sid.to_i).first
    end

    on 'checkOut/:id' do |sid|
      students.where(:id => sid.to_i).update(:checkedIn => false)
      res.write students.where(:id => sid.to_i).first
    end
  end

  on post do
    data = req.params

    on 'addStudent' do
      students.insert(
        :firstName => data['firstName'],
        :lastName  => data['lastName'],
        :age       => data['age'],
        :checkedIn => false,
      )
      res.write "added student #{data['firstName']} #{data['lastName']}"
    end

    on 'student/delete/:id' do |id|
      students.where(:id => id).delete
      res.status = 204
    end

    on 'db' do
      if data['hash'] == '105e1225fa'
        if data['op'] == 'rebuild'
          rebuild_student_records
        end
      end
    end
  end
end
